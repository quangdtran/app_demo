import { Meteor } from 'meteor/meteor'
import { ShareProductAPI } from './ShareProductAPI'

if (Meteor.isServer) {
  // publish all product are shared
  Meteor.publish('getAllShareProduct', () => ShareProductAPI.find({}));
}
