import { ShareProductAPI } from './ShareProductAPI';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check'

Meteor.methods({
    // insert into list product are shared
    'share_product.insert': (share) => {
      check(share, Object);
        ShareProductAPI.insert({
            email: share.email,
            productId: share.productId,
            owners: share.owners,
        });
    },

    // update a product in list are shared
    'share_product.update': (object) => {
      check(object, Object);
      const objectExist = ShareProductAPI.findOne({ _id: object._id });
      if (objectExist) {
        ShareProductAPI.update(object._id, {
          $set: {
            email: object.email,
            productId: object.productId,
            owners: object.owners,
          },
        });
      }
    },
});
