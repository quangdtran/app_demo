import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check'

if (Meteor.isServer) {
  // find user by email
  Meteor.publish('getUserByEmail', (email) => {
    check(email, String);
    const id = Accounts.findUserByEmail(email);
    return Meteor.users.find({ _id: id });
  });

  // get user current
  Meteor.publish('getCurrentUser', () => Meteor.users.find({ _id: Meteor.userId() }))
}
