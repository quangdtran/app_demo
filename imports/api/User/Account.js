import { Accounts } from 'meteor/accounts-base'

// Support for playing D&D: Roll 3d6 for dexterity.
Accounts.onCreateUser((options, user) => {
    const { emails } = user;
    emails[0].verified = true;
    const customizedUser = Object.assign(user, emails);

    // We still want the default hook's 'profile' behavior.
    if (options.profile) {
      customizedUser.profile = options.profile;
    }

    return customizedUser;
});
