import { Mongo } from 'meteor/mongo';

export const ProductAPI = new Mongo.Collection('products');
