import { Meteor } from 'meteor/meteor'
import { ProductAPI } from './ProductAPI'


// publish all product
if (Meteor.isServer) {
    Meteor.publish('getAllProduct', () => ProductAPI.find({}));
}
