import * as TYPES from './constants'

const initialState = {
    currentUser: null,
    isShare: false,
    productShared: null,
    isShowOwner: false,
    listOwnerShare: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
  case TYPES.GET_CURRENT_USER:
    return { ...state, currentUser: action.user }

  case TYPES.OPEN_SHARE_DIALOG:
    return { ...state, isShare: action.isShare, productShared: action.productShareId }

  case TYPES.CLOSE_SHARE_DIALOG:
    return { ...state, isShare: action.isShare }

  case TYPES.CLOSE_OWNER_DIALOG:
    return { ...state, isShowOwner: false }

  case TYPES.OPEN_OWNER_DIALOG: {
    return { ...state, isShowOwner: true, listOwnerShare: action.owners }
  }

  default:
    return state
  }
}
