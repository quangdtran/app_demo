import React, { Component } from 'react'
import { Button } from '@material-ui/core';
import {connect} from 'react-redux'
import {actionOpenShowOwnerDialog} from './action'

class ViewOwnerButton extends Component {
  constructor(props) {
    super(props)
  }

  // open list owner dialog
  openShowOwnerDialog() {
    let owners = this.props.data.owners;
    this.props.openShowOwnerDialog(owners)
  }

  render() {
    return (
      <div>
        <Button onClick={() => this.openShowOwnerDialog()} color="primary" variant="contained">View Owners</Button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    openShowOwnerDialog: (owners) => actionOpenShowOwnerDialog(dispatch, owners)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewOwnerButton);
