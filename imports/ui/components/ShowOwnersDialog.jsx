import React, { Component } from 'react'
import styled from 'styled-components'
import { Card, AppBar, FormGroup, TextField, Button } from '@material-ui/core';
import {connect} from 'react-redux'
import {actionCloseShowOwnerDialog, actionOpenShowOwnerDialog} from './action'
import { BlackScreen, Dialog, ListOwner, ShareButton } from './styled';

class ShowOwnersDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listOwner: []
        }
    }

    // get a list component display user shared product
    showOwner() {
        return this.props.owners.map((owner, index) => <p key={index+1}><i>{owner}</i></p>);
    }

  render() {
    return (
      <div>
        <BlackScreen onClick={() => this.props.closeShowOwner()}>
            <Dialog onClick={evt => evt.stopPropagation()}>
                <Card style={{
                    overflowY: "auto",
                    height: "100%"
                }}>
                    <AppBar position="static" style={{marginBottom: "1em", textAlign: "center", height: "3em", padding: "1em"}}>LIST OWNER SHARED</AppBar>
                    <ListOwner>
                        {this.showOwner()}
                    </ListOwner>
                </Card>
                <Card>
                    <ShareButton>
                        <Button color="primary" variant="contained" onClick={() => {
                            this.props.closeShowOwner();
                        }}>Close</Button>
                    </ShareButton>
                </Card>
            </Dialog>
        </BlackScreen>
      </div>
    )
  }
}

const mapStateToProps = state => {
    console.log(state.component.listOwnerShare);
    
    return {
        owners: state.component.listOwnerShare
    }
}
const mapDispatchToProps = dispatch => {
    return {
        closeShowOwner: () => actionCloseShowOwnerDialog(dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowOwnersDialog);