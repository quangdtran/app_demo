import React, { Component } from 'react'
import { Button } from '@material-ui/core';
import {connect} from 'react-redux';
import {
    actionAddLikedProduct,
    actionRemoveLikedProduct,
    actionOpenShareDialog
} from './action'
import { Meteor } from 'meteor/meteor';

class LikeShareButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: null
        }
    }

    // open share product dialog
    handleShareButton() {
        let product = this.props.data;
        this.props.openShareDialog(product)
    }

    // check and change state like or unlike of product
    handleLikeButton() {
        let productId = this.props.value;
        if (this.state.currentUser.profile) {
            if (this.state.currentUser.profile.product.liked.includes(productId)) {
                return this.props.removeLikedProduct(productId);
            }
        }
        this.props.addLikedProduct(productId);
    }

    componentDidMount() {
        this.setState({
            currentUser: Meteor.user()
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.currentUser) {
            this.setState({
                currentUser: nextProps.currentUser
            })
        }
    }

  render() {
    let checkLiked = false;
    if (this.state.currentUser) {
        let likedProducts = this.state.currentUser.profile.product.liked;
        if (likedProducts.includes(this.props.value)) {
            checkLiked = true;
        }
        
    }

    return (
      <div>
        <Button style={{width:"50%", color: checkLiked? "grey" : "blue"}} onClick={() => this.handleLikeButton()}>{checkLiked? "Unlike" : "Like"}</Button>
        <Button style={{width:"50%", color: "#ff00a5"}} onClick={() => this.handleShareButton()}>Share</Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.component.currentUser
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        // getCurrentUser: () => subscribeGetCurrentUser(dispatch),
        addLikedProduct: productId => actionAddLikedProduct(productId),
        removeLikedProduct: productId => actionRemoveLikedProduct(productId),
        openShareDialog: productId => actionOpenShareDialog(dispatch, productId)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LikeShareButton);