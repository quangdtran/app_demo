import styled from 'styled-components'

export const BlackScreen = styled.div`
    background-color: #0000005c;
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 99;
    top: 0;
    left: 0;
    cursor: pointer;
`;

export const Dialog = styled.div`
    position: fixed;
    z-index: 100;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -55%);
    width: 30%;
    height: 70%;
    background-color: white;
`;

export const ShareButton = styled.div`
    margin: 2em;
    float: right;
    clear: both;
`;

export const ListOwner = styled.div`
    margin-left: 1.5em;
    margin-top: 1em;
`;

export const UserCheckBox = styled.div`
    width: 100%;
`;

export const Title = styled.h3`
    color: black;
    text-align: center;
    margin: 1em 0em;
`;

export const Product = styled.div`
    margin-left: 1.5em;
    margin-top: 1em;
`;
