import { Meteor } from 'meteor/meteor'
import { Tracker } from 'meteor/tracker'
import * as TYPES from './constants'
import { ShareProductAPI } from '../../../api/ShareProduct/ShareProductAPI'

export const subscribeGetListShareProduct = (dispatch) => {
    const subsGetListShareProduct = Meteor.subscribe('getAllShareProduct', () => {
        Tracker.autorun((track) => {
            const listProduct = ShareProductAPI.find({
              email: Meteor.user().emails[0].address,
            }).fetch();
            if (subsGetListShareProduct.ready() && !track.firstRun) {
                console.log('Tracker.autorun subsGetAllTask ===> products');
                dispatch({
                    type: TYPES.GET_LIST_SHARE_PRODUCT,
                    products: listProduct || [],
                })
            }
        });
    });
}
