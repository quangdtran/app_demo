import * as TYPES from './constants'

const initialState = {
    listProductShared: [],
    listProduct: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.GET_LIST_SHARE_PRODUCT:
      return { ...state, listProductShared: action.products }

    default:
      return state
    }
}
