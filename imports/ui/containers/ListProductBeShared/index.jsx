import React, { Component } from 'react'
import { AgGridReact } from 'ag-grid-react';
import {connect} from 'react-redux';
import {subscribeGetListShareProduct} from './action';
import ViewOwnerButton from '../../components/ViewOwnerButton';
import { TableGrid } from './styled';

class ListProductBeShared extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [
                {headerName: "Name", field: "name", cellRenderer: params => `<p>${params.value}</p>`, cellStyle: {'white-space': 'normal'}},
                {headerName: "Brand", field: "brand"},
                {headerName: "Image", field: "image", cellRenderer: (params) => `<img style="width: 30%" src="${params.value}" />`},
                {headerName: "Price", field: "price"},
                {headerName: "Owner", field: "email", cellRendererFramework: (props) => <ViewOwnerButton {...props}/>, cellStyle: {paddingTop: "2em"}}
            ],
            listProduct: []
        }
    }

    componentWillMount() {
        this.props.getListShareProduct();
    }

    componentWillReceiveProps(nextProps) {
        if (!(nextProps.listProduct && nextProps.listShareProduct)) return;
        let newList = []
        nextProps.listProduct.forEach(product => {
            for (let share of nextProps.listShareProduct) {
                if (product.id === share.productId) {
                    newList.push({
                        name: product.name,
                        brand: product.brand,
                        image: product.image,
                        price: product.price,
                        owners: share.owners
                    });
                    break;
                }
            }
        });
        this.setState({listProduct: newList});
        
    }

  render() {
    return (
        <div>
            <TableGrid className="ag-theme-balham">
                <AgGridReact
                columnDefs={this.state.columnDefs}
                rowData={this.state.listProduct}
                rowHeight={100}
                
                onRowDoubleClicked={(p) => console.log(p)}
                ></AgGridReact>
            </TableGrid>
        </div>
    )
  }
}

const mapStateToProps = state => {
    return {
        listShareProduct: state.listProductBeShared.listProductShared
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getListShareProduct: () => subscribeGetListShareProduct(dispatch)
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(ListProductBeShared);