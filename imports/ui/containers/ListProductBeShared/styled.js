import styled from 'styled-components';

export const TableGrid = styled.div`
width: 80%;
position: relative;
left: 50%;
transform: translateX(-50%);
height: 400px;
display: inline-block;
margin-top: 0.5em;
`;
