import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import { AppBar, TextField, Card } from '@material-ui/core';
import { Meteor } from 'meteor/meteor';
import { Redirect } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const style = {
    width: '25em',
    marginBottom: '1em',
};

const styles = {
  root: {
    background: '#3f51b5',
    color: 'white',
  },
}

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isLogin: false,
        }
    }

    componentWillMount() {
        Meteor.logout((err) => {
            console.log(err);
        })
    }

    isChange(evt) {
        const { name, value } = evt.target;
        this.setState({ [name]: value });
    }

    handleClick(evt) {
        evt.preventDefault();
        const { email, password } = this.state;
        Meteor.loginWithPassword({ email }, password, (err) => {
            if (err) {
              alert('No Found User !!');
              return console.log(err);
            }
            console.log('userId: ', Meteor.user());
            this.setState({ isLogin: true });
            return null;
        })
    }

  render() {
    const { classes } = this.props;
    const { isLogin } = this.state;
    if (isLogin) return <Redirect push to="/list-product" />;
    return (
      <div>
        <Card style={{
          left: '50%',
          transform: 'translateX(-50%)',
          display: 'inline-block',
          position: 'relative',
          padding: '2em 10em',
          marginTop: '2em',
        }}
        >
          <div>
            <AppBar
              position="relative"
              style={{
                width: '25em',
                marginBottom: '3em',
                textAlign: 'center',
                padding: '0.5em',
                boxShadow: 'none',
              }}
            >
              Login
            </AppBar>

            <TextField
              label="Enter your Email"
              placeholder="Your Email"
              style={style}
              name="email"
              onChange={evt => this.isChange(evt)}
            />
            <br />
            <TextField
              type="password"
              label="Enter your Password"
              placeholder="Your Password"
              style={style}
              name="password"
              onChange={evt => this.isChange(evt)}
            />
            <br />

            <Button
              classes={{
                root: classes.root,
              }}
              label="Submit"
              variant="contained"
              color="primary"
              onClick={evt => this.handleClick(evt)}
            >
              Sign in
            </Button>
          </div>
        </Card>
      </div>
    )
  }
}

LoginForm.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
}

export default withStyles(styles)(LoginForm);
