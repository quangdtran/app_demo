import * as TYPES from './constants'

const initialState = {

}

export default (state = initialState, action) => {
  switch (action.type) {
  case TYPES:
    return { ...state }

  default:
    return state
  }
}
