import React, { Component } from 'react'
import {connect} from 'react-redux'
import {registerAction} from './action'
import { AppBar, TextField, Button, Card } from '@material-ui/core';
import {Redirect} from 'react-router-dom'

const style = {
    width: "25em",
    marginBottom: "1em"
};

class RegisterForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            email: "",
            password: "",
            confirmPassword: ""
        }
    }

    isChange(evt) {
        let name = evt.target.name;
        let value = evt.target.value;
        this.setState({[name]: value});
    }

    submitRegister(evt) {
        evt.preventDefault();
        let user = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }
        this.props.registerSubmit(user, () => {
            this.props.toLogin();
        });
    }

  render() {
    return (
        <div>
            <Card style={{
                left: "50%",
                transform: "translateX(-50%)",
                display: "inline-block",
                position: "relative",
                padding: "2em 10em",
                marginTop: "2em"
            }}>
                <div>
                    <AppBar
                        children="Register"
                        position="relative"
                        style={{
                            width: "25em",
                            marginBottom: "3em",
                            textAlign: "center",
                            padding: "0.5em",
                            boxShadow: "none"
                        }}
                        />
                    <TextField
                        label="Enter your name"
                        placeholder="Your Name"
                        style={style}
                        name="name"
                        onChange = {evt => {this.isChange(evt)}}
                        />
                    <br/>
                    <TextField
                        label="Enter your Email"
                        placeholder="Email"
                        style={style}
                        name="email"
                        onChange = {evt => {this.isChange(evt)}}
                        />
                    <br/>
                    <TextField
                    type="password"
                    label="Enter your Password"
                    placeholder="Password"
                    style={style}
                    name="password"
                    onChange = {evt => {this.isChange(evt)}}
                    />
                    <br/>
                    <TextField
                        type="password"
                        label="Enter Confirm Password"
                        placeholder="Confirm Password"
                        style={style}
                        name="confirmPassword"
                        onChange = {evt => {this.isChange(evt)}}
                        />
                    <br/>
                    <Button label="Submit" children="Submit" variant="contained" color="primary" onClick={ evt => this.submitRegister(evt)}/>
                </div>
            </Card>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        current: state.register.currentUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerSubmit: (user, cb) => {registerAction(dispatch, user, cb)}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);