import { Meteor } from 'meteor/meteor'
import * as TYPES from './constants'
import { registerValid } from '../../../helper/validation'

export const registerAction = (disptach, user, cb) => {
  const check = registerValid(user);
  if (!check.isValid) {
    return alert(check.message);
  }
  alert(check.message);
  Meteor.call('users.insert', user, (err, res) => {
    if (err) {
      console.log(err);
      return alert('Register is fail !!');
    }
    console.log(res);
    disptach({
      type: TYPES.REGISTER_ACOUNT,
      user,
    })
    alert('Register is success !');
    cb();
    return null;
  });

  return null;
}
