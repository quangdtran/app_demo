import * as TYPES from './constants'

const initialState = {
  currentUser: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.REGISTER_ACOUNT:
      return { ...state, currentUser: action.user }

    default:
      return state
  }
}
