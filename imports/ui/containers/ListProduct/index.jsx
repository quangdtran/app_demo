import React, { Component } from 'react'
import NavBar from '../../components/NavBar'
import {AgGridReact} from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import {
  subscribeGetAllProduct,
  subscribeGetCurrentUser,
  subscribeShareProduct
} from './action'
import { connect } from 'react-redux';
import ShareProductDialog from '../../components/ShareProductDialog';
import LikeShareButton from '../../components/LikeShareButton';
import ListProductBeShared from '../ListProductBeShared/index';
import ShowOwnersDialog from '../../components/ShowOwnersDialog'
import { Username, Title, TableGrid } from './styled';

const options = [
  'Choose a list',
  'List Of Product In Store',
  'List of products to be shared',
  'List of products you liked',
];

class ListProduct extends Component {

  constructor(props) {
    super(props)
    this.state = {
      listProduct: [],
      currentUser: {},
      listIndex: 1,
      isShare: false,
      isShowOwner: false,
      columnDefs: [
        {headerName: "Name", field: "name", cellRenderer: params => `<p>${params.value}</p>`, cellStyle: {'white-space': 'normal'}},
        {headerName: "Brand", field: "brand"},
        {headerName: "Image", field: "image", cellRenderer: (params) => `<img style="width: 30%" src="${params.value}" />`},
        {headerName: "Price", field: "price"},
        {headerName: "Feature", field: "id", cellRendererFramework: (props)=> <LikeShareButton {...props} />, cellStyle: {paddingTop: "2em"}}
      ]
    }
  }

  shareProduct(userIds) {
    let product = this.state.products.filter(p => {
      return p._id === this.state.productShareId;
    });
    console.log("product:", product);
    
    this.props.shareProduct(product, userIds);
  }

  selectListToFill(index) {
    if (index === 1) {
      this.setState({
        listIndex: index
      });
    }
    if (index === 2) {
      this.setState({listIndex: index});
    }
    else if (index === 3) {
      this.setState({listIndex: index});
    }
  }

  renderTable() {
    if (this.state.listIndex === 1) {
      return (
        <TableGrid className="ag-theme-balham">
          <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.listProduct}
            rowHeight={100}
            onRowDoubleClicked={(p) => console.log(p)}
          ></AgGridReact>
        </TableGrid>
      );
    }
    else if (this.state.listIndex === 2) {
      return (
        <ListProductBeShared listProduct={this.state.listProduct}/>
      );
    }
    else if (this.state.listIndex === 3) {
      let newList = this.state.listProduct.filter(product => {
        return this.state.currentUser.profile.product.liked.includes(product.id);
      });
      return (
        <TableGrid className="ag-theme-balham">
          <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={newList}
            rowHeight={100}
            onRowDoubleClicked={(p) => console.log(p)}
          ></AgGridReact>
        </TableGrid>
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      listProduct: nextProps.products,
      currentUser: nextProps.currentUser,
      isShare: nextProps.isShare,
      isShowOwner: nextProps.isShowOwner
    });
    this.selectListToFill(this.state.listIndex);
  }

  componentWillMount() {
    this.props.getAllProduct();
    this.props.getCurrentUser();
  }

  render() {
    let user = this.state.currentUser;
    return (
      <div>

          {this.state.isShare? <ShareProductDialog/> : null}
          {this.state.isShowOwner? <ShowOwnersDialog/> : null}
          <NavBar selectListToFill = {index => this.selectListToFill(index)}/>
          <Username>
            <i>Hello {Object.keys(user).length === 0? "" : user.profile.name}</i>
          </Username>
          <Title>{
            this.state.listIndex===1? options[this.state.listIndex] : (this.state.listIndex === 2)? options[this.state.listIndex] : options[this.state.listIndex]
          }</Title>
          {this.renderTable()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.listProduct.products,
    currentUser: state.listProduct.currentUser,
    listUser: state.listProduct.listUser,
    isShare: state.component.isShare,
    isShowOwner: state.component.isShowOwner
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getAllProduct: () => subscribeGetAllProduct(dispatch),
    getCurrentUser: () => subscribeGetCurrentUser(dispatch),
    shareProduct: (productId, userIds) => subscribeShareProduct(dispatch, productId, userIds)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListProduct);