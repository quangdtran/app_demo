import { Meteor } from 'meteor/meteor'
import * as TYPES from './constants'
import { Tracker } from 'meteor/tracker'
import { ProductAPI } from '../../../api/Product/ProductAPI'
import { ShareProductAPI } from '../../../api/ShareProduct/ShareProductAPI'
import { emailValid } from '../../../helper/validation'

const productsToRowData = (list) => {
    if (list !== []) {
      return list.map(product => ({
        name: product.meta.name,
        brand: product.meta.brand,
        image: product.image,
        price: product.meta.price,
        id: product._id._str,
      }));
    }
    return [];
}

export const subscribeGetAllProduct = (dispatch) => {
    const subsGetAllProduct = Meteor.subscribe('getAllProduct', () => {
        Tracker.autorun((track) => {
            let listProduct = ProductAPI.find({}).fetch();
            listProduct = productsToRowData(listProduct);
            if (subsGetAllProduct.ready() && !track.firstRun) {
                console.log('Tracker.autorun subsGetAllTask ===> products');
                dispatch({
                    type: TYPES.LOAD_ALL_PRODUCTS,
                    products: listProduct || [],
                })
            }
        })
    })
}

export const subscribeGetCurrentUser = (dispatch) => {
    const subsGetCurrentUser = Meteor.subscribe('getCurrentUser', () => {
        Tracker.autorun((track) => {
            const user = Meteor.users.findOne({});
            if (subsGetCurrentUser.ready() && !track.firstRun) {
                console.log('Tracker.autorun subsGetCurrentUser ===> current user');
                dispatch({
                    type: TYPES.GET_CURRENT_USER,
                    user: user || {},
                })
            }
        });
    });
}

export const subscribeShareProduct = (dispatch, email, product) => {
    Meteor.subscribe('getAllShareProduct', email, () => {
      if (!emailValid(email)) {
          return alert('Email invalid !!');
      }

      const listShare = ShareProductAPI.find({ email }).fetch();

      let check = false;
      let productIndex = -1;
      listShare.forEach((share, i) => {
          if (share.productId === product.id) {
              check = true;
              productIndex = i;
          }
      });
      const emailOwner = Meteor.user().emails[0].address;
      if (check) {
        if (!listShare[productIndex].owners.includes(emailOwner)) {
            listShare[productIndex].owners.push(emailOwner);
            Meteor.call('share_product.update', listShare[productIndex]);
            alert('Sharing is complete');
        } else {
            alert('Product is shared for ', emailOwner);
        }
      } else {
        Meteor.call('share_product.insert', { email, productId: product.id, owners: [emailOwner] }, (err) => {
            if (err) return console.log(err);
            return console.log('insert: ', emailOwner);
        });
        console.log('Da them product shared', listShare[productIndex]);
        alert('Sharing is complete');
      }
      return null;
    });
}
