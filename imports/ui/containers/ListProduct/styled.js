import styled from 'styled-components'

export const Username = styled.div`
display: inline-block;
position: relative;
top: 5em;
left: 82%;
`;

export const Title = styled.h1`
text-align: center;
color: #00205a;
margin-top: 2em;
`;

export const TableGrid = styled.div`
width: 80%;
position: relative;
left: 50%;
transform: translateX(-50%);
height: 400px;
display: inline-block;
margin-top: 0.5em;
`;
export const LikeBtn = styled.div`
width: 100%;
`;
