export const LOAD_ALL_PRODUCTS = 'LOAD_ALL_PRODUCTS';
export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const ADD_LIKED_PRODUCT = 'ADD_LIKED_PRODUCT';
export const REMOVE_LIKED_PRODUCT = 'REMOVE_LIKED_PRODUCT';
export const GET_ALL_USER = 'GET_ALL_USER';
