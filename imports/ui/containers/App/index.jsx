import React, { Component } from 'react'
import LoginForm from '../LoginForm/index';
import RegisterForm from '../RegisterForm/index';
import {Meteor} from 'meteor/meteor'
import { Button } from '@material-ui/core';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      isRegister: false
    }
    
  }

  toLogin() {
    this.setState({
      isLogin: true,
      isRegister: false
    });
  }

  componentDidMount() {
    console.log("check user: ", Meteor.userId && Meteor.userId() || null);
    
  }

  renderForm() {
    if ( this.state.isLogin ) return (<LoginForm/>);
    if ( this.state.isRegister ) return (<RegisterForm toLogin={() => this.toLogin()}/>);
    return (<div><h3 className="text-init">Click Button !!</h3></div>)
  }

  render() {
    return (
      <div>
        <div className="btn-option">
          <Button variant="contained" color="primary" onClick={() => { 
            this.setState({
              isRegister: true,
              isLogin: false
            }) 
          }}>Register</Button>
          <Button variant="contained" color="primary" onClick={() => {
            this.setState({
              isLogin: true,
              isRegister: false
            })
          }}>Login</Button>
        </div>
        {this.renderForm()}
      </div>
    )
  }
}

export default App;