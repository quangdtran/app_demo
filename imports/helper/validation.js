function response(isValid, msg) {
    return {
        isValid,
        message: msg,
    }
}

export function emailValid(email) {
  const re = new RegExp('^[a-z][a-z0-9_.]{5,32}@[a-z0-9]{2,}(.[a-z0-9]{2,4}){1,2}$');
  return re.test(email);
}

export function nameValid(name) {
  const re = new RegExp('^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$');
  return re.test(name);
}

export const passwordValid = password => (password.lengh <= 30)

export function registerValid(user) {
  if (user.name === '') return response(false, 'Name is empty !!');
  if (!nameValid(user.name)) return response(false, 'Name in invalid format');
  if (user.email === '') return response(false, 'Email is empty !!');
  if (!emailValid(user.email)) return response(false, 'Email Address in invalid format');
  if (!user.password) return response(false, 'password is empty !!');

  return response(true, 'Register is valid !')
}

export function loginValid(user) {
    if (user.email === '') return response(false, 'Email is empty !!');
    if (!emailValid(user.email)) return response(false, 'Email Address in invalid format');
    if (user.password) return response(false, 'password is empty !!');
    if (!passwordValid(user.password)) return response(false, 'password must less than 30 characters');
    return response(true, 'Login is valid !');
}
