import { combineReducers } from 'redux';
import registerReducer from '../../ui/containers/RegisterForm/reducer'
import listProductReducer from '../../ui/containers/ListProduct/reducer'
import componentReducer from '../../ui/components/reducer'
import listShareProductReducer from '../../ui/containers/ListProductBeShared/reducer'

// combine all reducer
export default combineReducers({
    register: registerReducer,
    listProduct: listProductReducer,
    component: componentReducer,
    listProductBeShared: listShareProductReducer,
});
