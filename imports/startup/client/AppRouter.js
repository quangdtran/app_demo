import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import { Provider } from 'react-redux';

import App from '../../ui/containers/App';

import stores from './MainStore';

import createHistory from 'history/createBrowserHistory';
import ListProduct from '../../ui/containers/ListProduct';
import LoginForm from '../../ui/containers/LoginForm';

const history = createHistory();
const store = stores();

// route app here...
function AppRouter() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/list-product" component={ListProduct} />
          <Route path="/login" component={LoginForm} />
        </Switch>
      </Router>
    </Provider>
  )
}
export default AppRouter;
