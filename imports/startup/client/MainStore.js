import { createStore } from 'redux';
import rootReducer from './MainReducer';

export default function configureStore(initState = {}) {
    // create root store
    const store = createStore(rootReducer, initState);
    return store;
}
